#!/bin/bash
# xc $NAME $HDD $SUB
# Dependencies: rpl libvirt
# Exit codes:
# 2: bad number of arguments
# 3: SSH agent forwarding not working

set -x -e

if [ $# -ne 3 ]; then echo 'Usage: xc $NAME $HDD $SUB'; exit 2; fi

# Check if SSH agent forwarding works
ssh-add -l || (echo 'Please use "ForwardAgent yes" in your ssh config!' && exit 3)

OURDIR=$(dirname $(which xc))
NAME=$1
HDD=$2
SUB=$3
MAC=`/usr/local/bin/macgen`
SSHPORT=$(expr 1 + $(grep listen /etc/haproxy/haproxy.cfg | awk -F ':' '{print $2}' | sort | tail -n 1))
IP=192.168.122.$(expr 1 + $(egrep -o '\.122\.[[:digit:]]*?' /etc/haproxy/haproxy.cfg | awk -F '.' '{print $3}' | sort -n | tail -n 1))

# Add $SUB to CIC DNS zone file
cd /etc/cic/dns/conf
git fetch --all 
git reset --hard origin/master 
echo "$SUB            IN      A       10.1.152.27" >> acdc.red.internal.conf
echo "$SUB            IN      A       \$IPREDATOR_IP" >> acdc.red.external.conf

## Sort the order of entries, except the header
for FOO in internal external; do
    head -n 4 acdc.red.$FOO.conf > acdc.red.$FOO.conf.tmp
    tail -n +5 acdc.red.$FOO.conf | sort >> acdc.red.$FOO.conf.tmp
    mv -v acdc.red.$FOO.conf.tmp acdc.red.$FOO.conf
done

git add acdc.red.internal.conf acdc.red.external.conf
git commit -m "Add A record for $SUB"
git push origin master

# Add $SUB to CIC proxy config
cd /etc/cic/proxy/sites-enabled
git fetch --all 
git reset --hard origin/master 
cp -v $OURDIR/subdomain.acdc.red $SUB.acdc.red
rpl -v '&SUBDOMAIN' $SUB $SUB.acdc.red
git add $SUB.acdc.red
git commit -m "Add (sub)domain to proxy config: $SUB"
git push origin master

# Documentation:
# http://wiki.libvirt.org/page/Networking#virsh_net-update
virsh net-update default add-last ip-dhcp-host \
    "<host mac='$MAC' name='$NAME' ip='$IP' />" \
    --live --config

# NOT WORKING -- WE DO MANUAL FIX ON NEXT LINE:
# virsh net-update default add dns-host \
#     "<host ip='$IP'> \
#        <hostname>$NAME</hostname> \
#      </host>" --live --config

echo "$IP $NAME" >> /etc/hosts

lxc-create -n $NAME -B lvm --vgname $HOSTNAME --fssize "$HDD"G -t debian -- -r jessie

echo "

lxc.network.type = veth
lxc.network.flags = up
lxc.network.link = virbr0
lxc.network.hwaddr = $MAC
lxc.network.ipv4 = 0.0.0.0/24
lxc.start.auto = 1
" >> /var/lib/lxc/$NAME/config

# Add subdomain to local haproxy configuration

echo "
        acl host_$SUB hdr(host) $SUB.acdc.red
        use_backend $SUB-http if host_$SUB

" >> /etc/haproxy/conf.d/00-global.cfg

echo "

backend $SUB-http
        server $SUB $IP:80 maxconn 32

listen  $NAME-ssh *:$SSHPORT
	mode tcp
	timeout client 4h
	timeout server 4h
	server $NAME $IP:22

" >> /etc/haproxy/conf.d/$IP-$NAME.cfg

haconfig

lxc-start -n $NAME -d
